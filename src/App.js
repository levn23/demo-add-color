import React, { useState } from "react";
import { ColorInput } from "./ColorInput";
import { ColorList } from "./ColorList";

function App() {
  const [colorCodes, setColorCodes] = useState([]);

  const addColorCodeHandler = colorCode => {
    setColorCodes([...colorCodes, colorCode]);
  };

  return (
    <div>
      <ColorInput addColorCodeHandler={addColorCodeHandler} />
      <ColorList colorCodes={colorCodes} />
    </div>
  );
}

export default App;

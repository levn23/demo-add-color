import React from "react";

const ColorRow = props => {
  return <div style={{ backgroundColor: props.colorCode }} />;
};

export const ColorList = props => {
  return (
    <div style={{ width: "200px" }}>
      {props.colorCodes.map((colorCode, index) => (
        <ColorRow key={index} colorCode={colorCode} />
      ))}
    </div>
  );
};

import React, { useState } from "react";

export const ColorInput = props => {
  const [codeColor, setColorCode] = useState("");

  return (
    <div>
      Color Code:
      <input
        value={codeColor}
        onChange={event => setColorCode(event.target.value)}
      />
      <button
        type="button"
        onClick={() => {
          props.addColorCodeHandler(codeColor);
          setColorCode("");
        }}
      >
        Add
      </button>
    </div>
  );
};
